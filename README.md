# dataminingcw

An increasing amount of online misinformation has motivated research in automated fact checking.
Your task in this project is to develop information retrieval and data mining methods to assess
the veracity of a claim. Specifically, the automated fact